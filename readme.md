This solution has the following assumptions:

1. routes are one-directional
    
    Example. 
    
    route: A - B - C
    
    is A directly connected with C? Answer: TRUE
    
    is C directly connected with A? Answer: FALSE
    
2. if depSid == arrSid then result of "direct connection" is FALSE

Assumptions are done because task description does not specify it explicitly.
