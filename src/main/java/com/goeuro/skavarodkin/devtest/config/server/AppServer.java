package com.goeuro.skavarodkin.devtest.config.server;

import com.goeuro.skavarodkin.devtest.exception.GoeuroException;
import com.goeuro.skavarodkin.devtest.rest.ConnectivityController;
import io.netty.handler.codec.http.HttpMethod;
import org.restexpress.RestExpress;
import org.restexpress.exception.BadRequestException;
import org.restexpress.serialization.SerializationProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * creates HTTP Server
 */
@Component
public class AppServer {

    private static final Logger LOGGER = LoggerFactory.getLogger(AppServer.class);

    private static final int PORT = 8088;
    private static final String CONNECTIVITY_URI = "/api/direct";

    @Autowired
    private ConnectivityController connectivityController;

    @Autowired
    private SerializationProvider serializationProvider;

    /**
     * start server and return it
     * @return server
     */
    public RestExpress startServer() {
        LOGGER.info("starting server");

        RestExpress.setDefaultSerializationProvider(serializationProvider);
        RestExpress server = new RestExpress();
        server.mapException(GoeuroException.class, BadRequestException.class);

        server.uri(CONNECTIVITY_URI, connectivityController)
                .method(HttpMethod.GET)
                .performSerialization()
        ;

        server.bind(PORT);
        return server;
    }

}
