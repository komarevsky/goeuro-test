package com.goeuro.skavarodkin.devtest.config.server;

import com.fasterxml.jackson.databind.module.SimpleModule;
import com.strategicgains.hyperexpress.domain.hal.HalResource;
import com.strategicgains.hyperexpress.serialization.jackson.HalResourceDeserializer;
import com.strategicgains.hyperexpress.serialization.jackson.HalResourceSerializer;
import org.restexpress.ContentType;
import org.restexpress.serialization.json.JacksonJsonProcessor;
import org.springframework.stereotype.Component;

/**
 * JSON processor
 */
@Component
public class GoeuroJsonSerializationProcessor extends JacksonJsonProcessor {

    public GoeuroJsonSerializationProcessor() {
        super();
        addSupportedMediaTypes(ContentType.HAL_JSON);
    }

    @Override
    protected void initializeModule(SimpleModule module) {
        super.initializeModule(module);

        // Support HalResource (de)serialization.
        module.addDeserializer(HalResource.class, new HalResourceDeserializer());
        module.addSerializer(HalResource.class, new HalResourceSerializer());
    }
}
