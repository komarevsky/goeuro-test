package com.goeuro.skavarodkin.devtest.config.server;

import org.restexpress.response.ErrorResponseWrapper;
import org.restexpress.response.ResponseWrapper;
import org.restexpress.serialization.AbstractSerializationProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * JSON serialization provider
 */
@Component
public class GoeuroSerializationProvider extends AbstractSerializationProvider {

    private static final ResponseWrapper RESPONSE_WRAPPER = new ErrorResponseWrapper();

    @Autowired
    public GoeuroSerializationProvider(GoeuroJsonSerializationProcessor processor) {
        super();
        add(processor, RESPONSE_WRAPPER, true);
    }
}
