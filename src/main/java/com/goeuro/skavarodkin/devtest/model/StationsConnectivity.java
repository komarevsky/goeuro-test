package com.goeuro.skavarodkin.devtest.model;

import java.util.LinkedList;
import java.util.List;

/**
 * Keeps the data about stations connectivity
 */
public class StationsConnectivity {

    // 0 element is route id; 1 element is id of first station in route
    private static final int FIRST_STATION_INDEX = 1;

    private static final int NOT_FOUND_INDEX = -1;

    // key is id of route; value - stations in route
    private List<int[]> routes;

    /**
     * creates new stations connectivity object
     */
    public StationsConnectivity() {
        routes = new LinkedList<>();
    }

    /**
     * adds new route
     * @param route first element is route id; next elements are ids of stations route goes through
     */
    public void addRoute(int[] route) {
        routes.add(route);
    }

    /**
     * checks if station with id {@code depSid} has direct route to station with id {@code arrSid}
     * @param depSid id of station
     * @param arrSid id of station
     * @return {@code true} if station with id {@code from} has direct route to station with id {@code arrSid}
     * otherwise returns {@code false}
     */
    public boolean existsDirectRouteBetweenStations(int depSid, int arrSid) {
        if (depSid == arrSid) {
            return false;
        }

        for (int[] route : routes) {
            int depSidIndex = NOT_FOUND_INDEX;
            int arrSidIndex = NOT_FOUND_INDEX;
            for (int i=FIRST_STATION_INDEX; i< route.length; i++) {
                int sid = route[i];
                if (sid == depSid) {
                    depSidIndex = i;
                } else if (sid == arrSid) {
                    arrSidIndex = i;
                }
            }

            if (depSidIndex < arrSidIndex && depSidIndex != NOT_FOUND_INDEX && arrSidIndex != NOT_FOUND_INDEX) {
                return true;
            }
        }

        return false;
    }

}
