package com.goeuro.skavarodkin.devtest.exception;

/**
 * Exception thrown by goeuro service
 */
public class GoeuroException extends RuntimeException {

    /**
     * constructor
     * @param message exception message
     */
    public GoeuroException(String message) {
        super(message);
    }

    /**
     * constructor
     * @param message message
     * @param cause cause
     */
    public GoeuroException(String message, Throwable cause) {
        super(message, cause);
    }
}
