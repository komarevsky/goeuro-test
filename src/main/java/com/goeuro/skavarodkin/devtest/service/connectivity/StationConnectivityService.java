package com.goeuro.skavarodkin.devtest.service.connectivity;

import com.goeuro.skavarodkin.devtest.model.StationsConnectivity;
import org.springframework.stereotype.Service;

/**
 * Service to determine if two stations directly connected
 */
@Service
public class StationConnectivityService {

    private StationsConnectivity stationsConnectivity;

    /**
     * default constructor
     */
    public StationConnectivityService() {
        this.stationsConnectivity = new StationsConnectivity();
    }

    /**
     * setup connectivity data
     * @param stationsConnectivity connectivity data
     */
    public void setStationsConnectivity(StationsConnectivity stationsConnectivity) {
        this.stationsConnectivity = stationsConnectivity;
    }

    /**
     * checks if station with id {@code depSid} has direct route to station with id {@code arrSid}
     * @param depSid id of station
     * @param arrSid id of station
     * @return {@code true} if station with id {@code from} has direct route to station with id {@code arrSid}
     * otherwise returns {@code false}
     */
    public boolean existsDirectRouteBetweenStations(int depSid, int arrSid) {
        return stationsConnectivity.existsDirectRouteBetweenStations(depSid, arrSid);
    }
}
