package com.goeuro.skavarodkin.devtest.service.initialisation;

import com.goeuro.skavarodkin.devtest.config.server.AppServer;
import com.goeuro.skavarodkin.devtest.model.StationsConnectivity;
import com.goeuro.skavarodkin.devtest.service.routedata.BusRouteFileParseService;
import com.goeuro.skavarodkin.devtest.service.connectivity.StationConnectivityService;
import com.goeuro.skavarodkin.devtest.service.cmdline.CmdArgsParseService;
import org.restexpress.RestExpress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Perform initializations required on app start
 */
@Service
public class AppStartupInitialisationService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AppStartupInitialisationService.class);

    @Autowired
    private CmdArgsParseService cmdArgsParseService;

    @Autowired
    private BusRouteFileParseService busRouteFileParseService;

    @Autowired
    private StationConnectivityService stationConnectivityService;

    @Autowired
    private AppServer appServer;

    /**
     * performs app initialisation activity
     * @param args command line arguments app is invoked with
     */
    public void initialize(String[] args) {
        LOGGER.info("trying to read and initialise route data");
        RestExpress server = appServer.startServer();
        String busRouteFile = cmdArgsParseService.getPathToRouteFile(args);
        StationsConnectivity connectivity = busRouteFileParseService.parseBusRouteFile(busRouteFile);
        stationConnectivityService.setStationsConnectivity(connectivity);
        LOGGER.info("initialisation completed");
        server.awaitShutdown();
    }

}
