package com.goeuro.skavarodkin.devtest.service.routedata;

import com.goeuro.skavarodkin.devtest.exception.GoeuroException;
import com.goeuro.skavarodkin.devtest.model.StationsConnectivity;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.NoSuchElementException;

/**
 * Service to parse file with bus route
 */
@Service
public class BusRouteFileParseService {

    private static final Logger LOGGER = LoggerFactory.getLogger(BusRouteFileParseService.class);

    private static final int PRINT_NUMBER_EVERY_NTH_LINE = 500;

    @Autowired
    private BusRouteLineProcessingService busRouteLineProcessingService;

    /**
     * parses file by path {@code filePath} and get information about stations connectivity from this file
     * @param filePath path to file with information about stations connectivity from this file
     * @return information about stations connectivity
     * @throws GoeuroException if file not exists or can not be read or has invalid format
     */
    public StationsConnectivity parseBusRouteFile(String filePath) throws GoeuroException {
        LOGGER.info("start reading from {}", filePath);

        StationsConnectivity stationsConnectivity;
        LineIterator it = null;

        try {
            File f = new File(filePath);
            it = FileUtils.lineIterator(f, "UTF-8");
            String line = it.nextLine();
            int routeNumber = Integer.parseInt(line);
            stationsConnectivity = new StationsConnectivity();

            for (int i=0; i<routeNumber; i++) {
                line = it.nextLine();
                if (i % PRINT_NUMBER_EVERY_NTH_LINE == 0) {
                    LOGGER.info("parsing {} line", i);
                }
                busRouteLineProcessingService.processLine(line, stationsConnectivity);
            }
        } catch (IOException | NoSuchElementException | NumberFormatException e) {
            throw new GoeuroException("can not read or parse file " + filePath, e);
        } finally {
            if (it != null) {
                LineIterator.closeQuietly(it);
            }
        }

        LOGGER.info("finish reading from {}", filePath);
        return stationsConnectivity;
    }
}
