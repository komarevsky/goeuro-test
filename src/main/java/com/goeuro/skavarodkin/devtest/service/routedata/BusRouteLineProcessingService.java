package com.goeuro.skavarodkin.devtest.service.routedata;

import com.goeuro.skavarodkin.devtest.model.StationsConnectivity;
import org.springframework.stereotype.Service;

import java.util.Arrays;

/**
 * service to process lines from bus route data file
 */
@Service
public class BusRouteLineProcessingService {

    public void processLine(String line, StationsConnectivity stationsConnectivity) {
        int[] route = Arrays.stream(line.split(" ")).mapToInt(Integer::parseInt).toArray();
        stationsConnectivity.addRoute(route);
    }
}
