package com.goeuro.skavarodkin.devtest.service.cmdline;

import com.goeuro.skavarodkin.devtest.exception.GoeuroException;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.stereotype.Service;

/**
 * Service to work with command line arguments
 */
@Service
public class CmdArgsParseService {

    /**
     * get path to file with route data
     * @param args command line arguments
     * @return path to file with route data
     * @throws GoeuroException if path to file can not be retrieved
     */
    public String getPathToRouteFile(String[] args) throws GoeuroException {
        if (ArrayUtils.isEmpty(args)) {
            throw new GoeuroException("Path to bus route file is not given. Expected to be first argument");
        }

        return args[0];
    }

}
