package com.goeuro.skavarodkin.devtest.rest.helper;

import com.goeuro.skavarodkin.devtest.exception.GoeuroException;
import org.apache.commons.lang3.StringUtils;
import org.restexpress.Request;
import org.springframework.stereotype.Component;

/**
 * Extracts params from request
 */
@Component
public class RequestParamExtractor {

    /**
     * extracts int value of parameter {@code paramName} from {@code request}
     * @param paramName name of parameter
     * @param request request
     * @return nt value of parameter {@code paramName} from {@code request}
     * @throws GoeuroException if parameter is missing or not integer
     */
    public int getRequiredIntParam(String paramName, Request request) throws GoeuroException {
        String paramString = request.getHeader(paramName);
        if (StringUtils.isEmpty(paramName)) {
            throw new GoeuroException("missing param " + paramName);
        }

        try {
            return Integer.parseInt(paramString);
        } catch (NumberFormatException ex) {
            throw new GoeuroException("value=" + paramString + " of param " + paramName + " is not integer");
        }
    }
}
