package com.goeuro.skavarodkin.devtest.rest;

import com.goeuro.skavarodkin.devtest.dto.DirectRouteExistenceDto;
import com.goeuro.skavarodkin.devtest.rest.helper.RequestParamExtractor;
import com.goeuro.skavarodkin.devtest.service.connectivity.StationConnectivityService;
import org.restexpress.Request;
import org.restexpress.Response;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static org.slf4j.LoggerFactory.getLogger;

/**
 * Controller which responses if 2 stations are directly connected
 */
@Component
public class ConnectivityController {

    private static final Logger LOGGER = getLogger(ConnectivityController.class);

    @Autowired
    private StationConnectivityService stationConnectivityService;

    @Autowired
    private RequestParamExtractor requestParamExtractor;

    /**
     * serves GET requests
     * @param request request
     * @param response response
     * @return information about connectivity
     */
    public DirectRouteExistenceDto read(Request request, Response response) {
        Integer depSid = requestParamExtractor.getRequiredIntParam("dep_sid", request);
        Integer arrSid = requestParamExtractor.getRequiredIntParam("arr_sid", request);
        LOGGER.debug("serving request dep_sid={}, arr_sid={}", depSid, arrSid);

        boolean directRouteExists = stationConnectivityService.existsDirectRouteBetweenStations(depSid, arrSid);
        return new DirectRouteExistenceDto(depSid, arrSid, directRouteExists);
    }

}
