package com.goeuro.skavarodkin.devtest;

import com.goeuro.skavarodkin.devtest.service.initialisation.AppStartupInitialisationService;
import org.slf4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import static org.slf4j.LoggerFactory.getLogger;

/**
 * Entry point to application
 */
public class Application {

    private static final Logger LOGGER = getLogger(Application.class);

    public static void main(String[] args) {
        LOGGER.info("Initialising context");
        ApplicationContext ctx =new AnnotationConfigApplicationContext("com.goeuro.skavarodkin.devtest");
        LOGGER.info("Context initialized");

        AppStartupInitialisationService appStartupInitialisationService =
                ctx.getBean(AppStartupInitialisationService.class);

        appStartupInitialisationService.initialize(args);
    }
}
